## Compile options
Slimlog contains a few optional flags for adjusting the internals.

* `EXT_NO_USE_STDERR` - integer:
		if greater than 0 only use `stdout` for console.
* `EXT_DEBUG_DISABLED_FLAG`  - integer:
		if greater than 0 completely disable debug log messages.
* `EXT_LOGGING_DISABLED_FLAG` - integer:
		if greater than 0 completely disable logging altogether.
* `EXT_LOG_LEVEL_OVERRIDE` - integer:
		if greater than 0 resolve to the internal log-level for overriding what
		the minimum log-level is. This number must be between 0 and 6.


## General usage
The only module you need to import is `slimlog`.

After import, you must instantiate a new instance of Logger via `newLogger`.
You have a large amount of control over how this works, but by default it will
create a logger with the given provider name at the provided minimum log level
which can be overridden via a compile flag (see above) and a single console
sink with colors enabled using the default callback. I suggest using named
function arguments due to the sheer volume of them.

From here, you can add more sinks by calling the given functions `add_*sinks`
which takes a variable number of sinks to add.

You can either manually call emit, or use one of the helper functions that
hand it a given `LogLevel` value, such as `debug`.

