import slimlog

let log = newLogger("test logger")
log.add(newFileSink("test.log"))
log.add(newSyslogSink("logtest"))

log.debug("This is a debug message")
log.add(newConsoleSink(false))
log.debug("Added non-color console sink to test log")

log.close()

