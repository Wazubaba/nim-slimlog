import defaults/consoledefaults
import defaults/filedefaults
import defaults/syslogdefaults

import times
import sinks

type
  Callback* = ref CallbackObj ## Implements a container for the 3 required callbacks

  CallbackObj = object
    ## Callback for use on a FileSink
    fcallback*: proc (sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`])
    ## Callback for use on a ConsoleSink
    ccallback*: proc (sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`])
    ## Callback for use on a SystemSink
    scallback*: proc (sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`])

proc newCallback*(
  file_callback: proc (sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]),
  console_callback: proc (sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]),
  system_callback: proc (sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`])): Callback =
  ## Create a new callback object
  return Callback(fcallback: file_callback, ccallback: console_callback, scallback: system_callback)

proc route*(self: Callback, sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]) =
  ## Simplifies routing to ensure the proper callbacks are called for a given sink
  case sink.target:
    of Target.Console:
      self.ccallback(sink, timestamp, provider, message)
    of Target.File:
      self.fcallback(sink, timestamp, provider, message)
    of Target.Syslog:
      self.scallback(sink, timestamp, provider, message)
  

let
  DEFAULT_MESSAGE_CALLBACK* = Callback(
    fcallback: filedefaults.DEFAULT_MESSAGE_MESSAGE_CALLBACK,
    ccallback: consoledefaults.DEFAULT_MESSAGE_MESSAGE_CALLBACK,
    scallback: syslogdefaults.DEFAULT_MESSAGE_MESSAGE_CALLBACK
  ) ## Default callback for use with messages

  DEFAULT_DEBUG_CALLBACK* = Callback(
    fcallback: filedefaults.DEFAULT_MESSAGE_DEBUG_CALLBACK,
    ccallback: consoledefaults.DEFAULT_MESSAGE_DEBUG_CALLBACK,
    scallback: syslogdefaults.DEFAULT_MESSAGE_DEBUG_CALLBACK
  ) ## Default callback for use with debug

  DEFAULT_INFO_CALLBACK* = Callback(
    fcallback: filedefaults.DEFAULT_MESSAGE_INFO_CALLBACK,
    ccallback: consoledefaults.DEFAULT_MESSAGE_INFO_CALLBACK,
    scallback: syslogdefaults.DEFAULT_MESSAGE_INFO_CALLBACK
  ) ## Default callback for use with info

  DEFAULT_WARN_CALLBACK* = Callback(
    fcallback: filedefaults.DEFAULT_MESSAGE_WARN_CALLBACK,
    ccallback: consoledefaults.DEFAULT_MESSAGE_WARN_CALLBACK,
    scallback: syslogdefaults.DEFAULT_MESSAGE_WARN_CALLBACK
  ) ## Default callback for use with warn

  DEFAULT_ERROR_CALLBACK* = Callback(
    fcallback: filedefaults.DEFAULT_MESSAGE_ERROR_CALLBACK,
    ccallback: consoledefaults.DEFAULT_MESSAGE_ERROR_CALLBACK,
    scallback: syslogdefaults.DEFAULT_MESSAGE_ERROR_CALLBACK
  ) ## Default callback for use with error

  DEFAULT_FATAL_CALLBACK* = Callback(
    fcallback: filedefaults.DEFAULT_MESSAGE_FATAL_CALLBACK,
    ccallback: consoledefaults.DEFAULT_MESSAGE_FATAL_CALLBACK,
    scallback: syslogdefaults.DEFAULT_MESSAGE_FATAL_CALLBACK
  ) ## Default callback for use with fatal

  DEFAULT_CRITICAL_CALLBACK* = Callback(
    fcallback: filedefaults.DEFAULT_MESSAGE_CRITICAL_CALLBACK,
    ccallback: consoledefaults.DEFAULT_MESSAGE_CRITICAL_CALLBACK,
    scallback: syslogdefaults.DEFAULT_MESSAGE_CRITICAL_CALLBACK
  ) ## Default callback for use with critical

  DEFAULT_TIMESTAMP_CALLBACK* = getClockStr ## Default callback to get a timestamp

