import strutils
import ../sinks

proc DEFAULT_MESSAGE_DEBUG_CALLBACK*(sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]) =
  sink.emit(LOG_DEBUG, timestamp(), " DEBUG [", provider & "]: ", message.join(" "))

proc DEFAULT_MESSAGE_ERROR_CALLBACK*(sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]) =
  sink.emit(LOG_ERR, timestamp(), " ERROR [", provider & "]: ", message.join(" "))

proc DEFAULT_MESSAGE_INFO_CALLBACK*(sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]) =
  sink.emit(LOG_INFO, timestamp(), " INFO [", provider & "]: ", message.join(" "))

proc DEFAULT_MESSAGE_MESSAGE_CALLBACK*(sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]) =
  sink.emit(LOG_INFO, timestamp(), " [", provider & "]: ", message.join(" "))

proc DEFAULT_MESSAGE_WARN_CALLBACK*(sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]) =
  sink.emit(LOG_WARNING, timestamp(), " WARN [", provider & "]: ", message.join(" "))

proc DEFAULT_MESSAGE_FATAL_CALLBACK*(sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]) =
  sink.emit(LOG_CRIT, timestamp(), " FATAL [", provider & "]: ", message.join(" "))

proc DEFAULT_MESSAGE_CRITICAL_CALLBACK*(sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]) =
  sink.emit(LOG_EMERG, timestamp(), " CRITICAL [", provider & "]: ", message.join(" "))
