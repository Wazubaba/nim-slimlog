import strutils
import ../sinks

proc DEFAULT_MESSAGE_DEBUG_CALLBACK*(sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]) =
  sink.emit(timestamp(), " DEBUG [", provider & "]: ", message.join(" "))

proc DEFAULT_MESSAGE_ERROR_CALLBACK*(sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]) =
  sink.emit(timestamp(), " ERROR [", provider & "]: ", message.join(" "))

proc DEFAULT_MESSAGE_INFO_CALLBACK*(sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]) =
  sink.emit(timestamp(), " INFO [", provider & "]: ", message.join(" "))

proc DEFAULT_MESSAGE_MESSAGE_CALLBACK*(sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]) =
  sink.emit(timestamp(), " [", provider & "]: ", message.join(" "))

proc DEFAULT_MESSAGE_WARN_CALLBACK*(sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]) =
  sink.emit(timestamp(), " WARN [", provider & "]: ", message.join(" "))

proc DEFAULT_MESSAGE_FATAL_CALLBACK*(sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]) =
  sink.emit(timestamp(), " FATAL [", provider & "]: ", message.join(" "))

proc DEFAULT_MESSAGE_CRITICAL_CALLBACK*(sink: Sink, timestamp: proc(): string, provider: string, message: varargs[string, `$`]) =
  sink.emit(timestamp(), " CRITICAL [", provider & "]: ", message.join(" "))

