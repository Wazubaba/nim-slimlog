# S L I M L O G
Yet another attempt at reinventing the wheel of logging in Nim.

Slimlog attempts to be extremely configurable (every method of outputting
information can be configured) while still having sane defaults so that it
can be used with next to no effort.

This was made because the next best alternative, `nim-chronicals`, is a total
bloated (albeit awesome) monstrosity.

